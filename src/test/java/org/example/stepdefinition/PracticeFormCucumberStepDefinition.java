package org.example.stepdefinition;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.example.model.PracticeFormModel;
import org.example.page.PracticeFormPage;
import org.example.setup.WebUI;
import org.example.util.Gender;
import org.example.util.Hobbies;
import org.junit.jupiter.api.Assertions;

import java.util.ArrayList;
import java.util.List;

public class PracticeFormCucumberStepDefinition extends WebUI {

    private PracticeFormModel maria;
    private PracticeFormModel marcos;
    private PracticeFormPage practiceFormPage ;
    private static final String ASSERTION_EXCEPTION_MESSAGE = "Los valores suministrados no son los  esperados: %s";


    //Background:
    @Given("el empleado adminisrtativo se encuantra en la pagina wen de los ingreso de estudiantes")
    public void elEmpleadoAdminisrtativoSeEncuantraEnLaPaginaWenDeLosIngresoDeEstudiantes() {

        try {
            generalSetup();
        }catch (Exception e){
            System.out.println("error catch beforeEach");
            quiteDriver();

        }

    }
    //First scenario:
    @When("el empleado administrativo ingresa los campos obligatorios y confirma la acción")
    public void elEmpleadoAdministrativoIngresaLosCamposObligatoriosYConfirmaLaAcción() {

        try {
            generateStudentMaria();
            practiceFormPage = new PracticeFormPage(maria, driver);
            practiceFormPage.fillMandatoryFields();

            Assertions.assertEquals(
                    practiceFormPage.isResgistrationDone(),
                    forSubmittedForm(),
                    String.format(ASSERTION_EXCEPTION_MESSAGE,outcome()));
        }catch (Exception e){
            System.out.println("error en en Test");
            quiteDriver();
        }

    }

    @Then("el sistema debera mostrar por pantalla el registro del estudiante ingresado.")
    public void elSistemaDeberaMostrarPorPantallaElRegistroDelEstudianteIngresado() {

        try {
            Assertions.assertEquals(
                    practiceFormPage.isResgistrationDone(),
                    forSubmittedForm(),
                    String.format(ASSERTION_EXCEPTION_MESSAGE,outcome()));
                    quiteDriver();

        }catch (Exception e){
            System.out.println("error en en Test");
            quiteDriver();
        }

    }

    //second scenario
    @When("el empleado administrativo ingresa los campos obligatorios, otros adicionales y confirma")
    public void elEmpleadoAdministrativoIngresaLosCamposObligatoriosOtrosAdicionalesYConfirma() {
        try {
            generateStudentMarcos();
            practiceFormPage = new PracticeFormPage(marcos, driver);
            practiceFormPage.fillMandatoryFieldsAndOthers();

        }catch (Exception e){
            System.out.println("error elEmpleadoAdministrativoIngresaLosCamposObligatoriosOtrosAdicionalesYConfirma");
            quiteDriver();
        }

    }
    @Then("el sistema debera mostrar por pantalla el registro mas los campos alternos.")
    public void elSistemaDeberaMostrarPorPantallaElRegistroMasLosCamposAlternos() {
        try {
            Assertions.assertEquals(
                    practiceFormPage.isResgistrationDoneForMarcos(),
                    forSubmittedFormForMarcos(),
                    String.format(ASSERTION_EXCEPTION_MESSAGE,outcome2()));
            quiteDriver();

        }catch (Exception e){
            System.out.println("error en elSistemaDeberaMostrarPorPantallaElRegistroMasLosCamposAlternos");
            quiteDriver();
        }

    }


//Help Functions
    private void generateStudentMaria(){
        maria = new PracticeFormModel();
        maria.setName("Maria");
        maria.setLastName("Mora");
        maria.setGender(Gender.FEMALE);
        maria.setMobileNumber("3502018875");
    }
    public List<String> forSubmittedForm(){
        List<String> submitedFormResult = new ArrayList<>();
        submitedFormResult.add(maria.getName()+" "+maria.getLastName());
        submitedFormResult.add(maria.getGender().getValue());
        submitedFormResult.add(maria.getMobileNumber());
        return submitedFormResult;
    }
    public List<String> forSubmittedFormForMarcos(){
        List<String> submitedFormResult = new ArrayList<>();
        submitedFormResult.add(marcos.getName()+" "+marcos.getLastName());
        submitedFormResult.add(marcos.getGender().getValue());
        submitedFormResult.add(marcos.getMobileNumber());
        submitedFormResult.add(marcos.getDay()+" "+marcos.getMonth()+","+marcos.getYear());
        submitedFormResult.add(marcos.getHobbies().getValue());


        return submitedFormResult;
    }
    private String outcome(){
        return "\n"+ practiceFormPage.isResgistrationDone().toString() + "\n\r" + forSubmittedForm().toString();
    }
    private String outcome2(){
        return "\n"+ practiceFormPage.isResgistrationDoneForMarcos().toString() + "\n\r" + forSubmittedFormForMarcos().toString();
    }
    private void generateStudentMarcos(){
        marcos = new PracticeFormModel();
        marcos.setName("Marcos");
        marcos.setLastName("Mora");
        marcos.setGender(Gender.MALE);
        marcos.setMobileNumber("3117657555");
        marcos.setMonth("November");
        marcos.setDay("04");
        marcos.setYear("1992");
        marcos.setHobbies(Hobbies.READING);

    }

}

