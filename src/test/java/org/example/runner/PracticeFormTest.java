package org.example.runner;

import org.example.model.PracticeFormModel;
import org.example.page.PracticeFormPage;
import org.example.setup.WebUI;
import org.example.util.Gender;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class PracticeFormTest extends WebUI {

    private PracticeFormModel maria;
    private PracticeFormPage practiceFormPage ;

    private static final String ASSERTION_EXCEPTION_MESSAGE = "Los valores suministrados no son lso esperados: %s";

    @BeforeEach
    public void  setUp(){
        try {
            generateStudent();
            generalSetup();
        }catch (Exception e){
            System.out.println("error catch beforeEach");
            quiteDriver();

        }
    }

    @Test
    public void practiceFormTestMandatoryFields(){
        try {
            practiceFormPage = new PracticeFormPage(maria, driver);
            practiceFormPage.fillMandatoryFields();

            Assertions.assertEquals(
                    practiceFormPage.isResgistrationDone(),
                    forSubmittedForm(),
                    String.format(ASSERTION_EXCEPTION_MESSAGE,outcome()));
        }catch (Exception e){
            System.out.println("error en en Test");
            quiteDriver();
        }
    }

    @AfterEach
    public void tearDown(){
        quiteDriver();
    }


    private void generateStudent(){
        maria = new PracticeFormModel();
        maria.setName("Maria");
        maria.setLastName("Mora");
        maria.setGender(Gender.FEMALE);
        maria.setMobileNumber("3502018875");
    }

    public List<String> forSubmittedForm(){
        List<String> submitedFormResult = new ArrayList<>();
        submitedFormResult.add(maria.getName()+" "+maria.getLastName());
        submitedFormResult.add(maria.getGender().getValue());
        submitedFormResult.add(maria.getMobileNumber());

        return submitedFormResult;
    }

    private String outcome(){
        return "\n"+ practiceFormPage.isResgistrationDone().toString() + "\n\r" + forSubmittedForm().toString();
    }

}
