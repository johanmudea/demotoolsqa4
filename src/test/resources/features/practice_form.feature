Feature: Formulario de estudiante
  Como empleado administrativo necesito ingresar al sistema los estudiantes
  de los nuevos ciclos academicos con el fin de cumplir las politicas
  administrativas y de autitoría de la universidad.

  Background:
    Given el empleado adminisrtativo se encuantra en la pagina wen de los ingreso de estudiantes

  @t1
  Scenario: Ingreso un estudiante con los campos obligatorios
    When el empleado administrativo ingresa los campos obligatorios y confirma la acción
    Then  el sistema debera mostrar por pantalla el registro del estudiante ingresado.

  @t2
  Scenario: Ingreso un estudiante con los campos de la forma alterna
    When el empleado administrativo ingresa los campos obligatorios, otros adicionales y confirma
    Then  el sistema debera mostrar por pantalla el registro mas los campos alternos.

