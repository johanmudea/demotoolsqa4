package org.example.page.common;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

public class CommonActionOnPages {

    private  WebDriver webDriver;

    protected CommonActionOnPages(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    protected void typeInto(By locator, String value){
        webDriver.findElement(locator).sendKeys(value);

    }

    protected  void clearText(By locator){
        webDriver.findElement(locator).clear();

    }

    protected void click(By locator){
        webDriver.findElement(locator).click();

    }

    protected void scroll(By locator){
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("window.scrollBy(0,1000)");
        //js.executeScript("arguments[0].scrollIntoView();", locator);

    }

    protected String getText(By locator){
        return webDriver.findElement(locator).getText();
    }

}
