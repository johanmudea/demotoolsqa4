package org.example.page;

import org.example.model.PracticeFormModel;
import org.example.page.common.CommonActionOnPages;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;
import java.util.List;


public class PracticeFormPage extends CommonActionOnPages {

    private final PracticeFormModel practiceFormModel;

    //En el page colocamos los inputs y localizadores.
    private final By name = By.id("firstName");
    private final By lastName = By.id("lastName");

    private final By genderMale = By.xpath("//*[@id=\"genterWrapper\"]/div[2]/div[1]/label");
    private final By genderFemale = By.xpath("//*[@id=\"genterWrapper\"]/div[2]/div[2]/label");
    private final By genderOther = By.xpath("//*[@id=\"genterWrapper\"]/div[2]/div[3]/label");
    private final By mobileNumber = By.xpath("//*[@id='userNumber']");

    private final By submitButton = By.xpath("//*[@id=\"submit\"]");

    private final By hobbiesSports = By.xpath("//*[@id=\"hobbiesWrapper\"]/div[2]/div[1]");
    private final By hobbiesReading = By.xpath("//*[@id=\"hobbiesWrapper\"]/div[2]/div[2]");
    private final By hobbiesMusic = By.xpath("//*[@id=\"hobbiesWrapper\"]/div[2]/div[3]");

    private final By dateOfBirthInput = By.id("dateOfBirthInput");
    private final By dateOfBirthYear = By.xpath("//option[. ='1992']");
    private final By dateOfBirthMonth = By.xpath("//option[. ='November']");
    private final By dateOfBirthDay = By.xpath("//div[contains(@aria-label,'4') and contains(@aria-label,'November') and contains(@aria-label,'Wednesday')]");


    //Para validaciones
    private final By assertionName = By.xpath("/html/body/div[5]/div/div/div[2]/div/table/tbody/tr[1]/td[2]");
    private final By assertionGender = By.xpath("/html/body/div[5]/div/div/div[2]/div/table/tbody/tr[3]/td[2]");
    private final By assertionMobile = By.xpath("/html/body/div[5]/div/div/div[2]/div/table/tbody/tr[4]/td[2]");

    private final By assertiondate = By.xpath("/html/body/div[5]/div/div/div[2]/div/table/tbody/tr[5]/td[2]");
    private final By assertionHobbies = By.xpath("/html/body/div[5]/div/div/div[2]/div/table/tbody/tr[7]/td[2]");


    //Tamnbien colocamos las acciones. auqnue puedo crar una clase de common action pages primero en un paquete dentro del paquete page.

    public PracticeFormPage(PracticeFormModel practiceFormModel, WebDriver webDriver) {
        super(webDriver);
        this.practiceFormModel = practiceFormModel;
    }

    public void fillMandatoryFields(){
        clearText(name);
        typeInto(name,practiceFormModel.getName());
        clearText(lastName);
        typeInto(lastName,practiceFormModel.getLastName());
        switch (practiceFormModel.getGender()){
            case FEMALE:
                click(genderFemale);
                break;
            case MALE:
                click(genderMale);
                break;
            case OTHER:
                click(genderOther);
                break;
            default:
        }
        clearText(mobileNumber);
        typeInto(mobileNumber,practiceFormModel.getMobileNumber());


        scroll(submitButton);
        click(submitButton);

    }

    public void fillMandatoryFieldsAndOthers(){
        clearText(name);
        typeInto(name,practiceFormModel.getName());
        clearText(lastName);
        typeInto(lastName,practiceFormModel.getLastName());
        switch (practiceFormModel.getGender()){
            case FEMALE:
                click(genderFemale);
                break;
            case MALE:
                click(genderMale);
                break;
            case OTHER:
                click(genderOther);
                break;
            default:
        }
        clearText(mobileNumber);
        typeInto(mobileNumber,practiceFormModel.getMobileNumber());
        switch (practiceFormModel.getHobbies()){
            case SPORTS:
                click(hobbiesSports);
                break;
            case READING:
                click(hobbiesReading);
                break;
            case MUSIC:
                click(hobbiesMusic);
                break;
            default:
        }
        click(dateOfBirthInput);
        click(dateOfBirthYear);
        click(dateOfBirthMonth);
        click(dateOfBirthDay);

        scroll(submitButton);
        click(submitButton);

    }

    public List<String> isResgistrationDone(){
        List<String> submitedFormResult = new ArrayList<>();
        submitedFormResult.add(getText(assertionName).trim());
        submitedFormResult.add(getText(assertionGender).trim());
        submitedFormResult.add(getText(assertionMobile).trim());
        return submitedFormResult;
    }

    public List<String> isResgistrationDoneForMarcos(){
        List<String> submitedFormResult = new ArrayList<>();
        submitedFormResult.add(getText(assertionName).trim());
        submitedFormResult.add(getText(assertionGender).trim());
        submitedFormResult.add(getText(assertionMobile).trim());
        submitedFormResult.add(getText(assertiondate).trim());
        submitedFormResult.add(getText(assertionHobbies).trim());
        return submitedFormResult;
    }



}
